var through = require('through');
var File = require('vinyl');
var fs = require('fs');

var svg2css = function(out, options) {
	options = options || {};

	var files = [];

	var onFile = function(file) {
		if (fs.lstatSync(file.path).isFile()) files.push(file);
	}

	var onEnd = function() {
		var file = new File({
			cwd: './',
			path: out,
			contents: new Buffer(files.map(function(p) {
				var path = !options.absolute ? p.path.replace(new RegExp('^' + process.cwd() + '/'), ''): p.path;
				return '.' + p.relative.replace(/\.svg/gi, '') + ' {  background-image: url('+path+');\r\nbackground-size: 100% 100%; }'
			}).join('\r\n'))
		})

		this.emit('data', file);
		this.emit('end');
	}

	return through(onFile, onEnd);
}

module.exports = svg2css;